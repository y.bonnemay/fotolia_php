#ifndef PHP_FOTOLIA_VANILLA_H
#define PHP_FOTOLIA_VANILLA_H

#define PHP_FOTOLIA_VANILLA_EXTNAME  "fotolia_vanilla"
#define PHP_FOTOLIA_VANILLA_EXTVER   "0.1"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif 

#include <curl/curl.h>
extern "C" {
#include "php.h"
}


extern zend_module_entry fotolia_vanilla_module_entry;
#define phpext_fotolia_vanilla_ptr &fotolia_vanilla_module_entry;

#endif /* PHP_FOTOLIA_VANILLA_H */
