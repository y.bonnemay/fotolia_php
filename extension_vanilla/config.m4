PHP_ARG_ENABLE(fotolia_vanilla,
    [Whether to enable the "fotolia_vanilla" extension],
    [  --enable-fotolia_vanilla      Enable "fotolia_vanilla" extension support])

if test $PHP_FOTOLIA_VANILLA != "no"; then
    PHP_REQUIRE_CXX()
    PHP_SUBST(FOTOLIA_VANILLA_SHARED_LIBADD)
    
    PHP_ADD_LIBRARY_WITH_PATH(curl, /usr/lib/x86_64-linux-gnu, FOTOLIA_VANILLA_SHARED_LIBADD)
    AC_DEFINE(HAVE_CURL,1,[curl found and included])      
    PHP_ADD_LIBRARY(stdc++, 1, FOTOLIA_VANILLA_SHARED_LIBADD)
    
    PHP_NEW_EXTENSION(fotolia_vanilla, fotolia_vanilla.cc fotolia_cpp.cxx, $ext_shared)
    CFLAGS="${CFLAGS} -g -O0"
fi

