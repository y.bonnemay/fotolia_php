#include "php_fotolia_vanilla.h"

////////////
// Counting references 
#include "fotolia_cpp.h"

zend_object_handlers fotolia_api_object_handlers;

struct fotolia_api_object {
    zend_object std;
    Fotolia_Api *fotolia_api;
};

void fotolia_api_free_storage(void *object TSRMLS_DC)
{
    fotolia_api_object *obj = (fotolia_api_object *)object;
    delete obj->fotolia_api; 

    zend_hash_destroy(obj->std.properties);
    FREE_HASHTABLE(obj->std.properties);

    efree(obj);
}

zend_object_value fotolia_api_create_handler(zend_class_entry *type TSRMLS_DC)
{
    zval *tmp;
    zend_object_value retval;

    fotolia_api_object *obj = (fotolia_api_object *)emalloc(sizeof(fotolia_api_object));
    memset(obj, 0, sizeof(fotolia_api_object));
    obj->std.ce = type;

    ALLOC_HASHTABLE(obj->std.properties);
    zend_hash_init(obj->std.properties, 0, NULL, ZVAL_PTR_DTOR, 0);;
    object_properties_init(&obj->std, type);

    retval.handle = zend_objects_store_put(obj, NULL,
        fotolia_api_free_storage, NULL TSRMLS_CC);
    retval.handlers = &fotolia_api_object_handlers;

    return retval;
}
////////////

////////////
// Glue for the PHP
zend_class_entry *fotolia_api_ce;
ZEND_METHOD(Fotolia_Api, __construct)
{
    char *string_key = NULL;
    uint string_key_len = 0;
    Fotolia_Api *fotolia_api = NULL;
    zval *object = getThis();

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
                              "s",
                              &string_key,
                              &string_key_len) == FAILURE) {
        RETURN_NULL();
    }
    
    std::string key(string_key);
    fotolia_api = new Fotolia_Api(key);
    fotolia_api_object *obj = (fotolia_api_object *)zend_object_store_get_object(object TSRMLS_CC);
    obj->fotolia_api = fotolia_api;
}

ZEND_METHOD(Fotolia_Api, getApiKey)
{
    Fotolia_Api *fotolia_api;
    fotolia_api_object *obj = (fotolia_api_object *)zend_object_store_get_object(
        getThis() TSRMLS_CC);
    fotolia_api = obj->fotolia_api;
    if (fotolia_api != NULL) {
        std::string apiKey(fotolia_api->getApiKey());
        RETURN_STRING(apiKey.c_str(), 1);
    }
    RETURN_NULL();
}



ZEND_METHOD(Fotolia_Api, _api)
{

    Fotolia_Api *fotolia_api;
    fotolia_api_object *obj = (fotolia_api_object *)zend_object_store_get_object(
        getThis() TSRMLS_CC);
    fotolia_api = obj->fotolia_api;
    if (fotolia_api == NULL) {
        RETURN_NULL();
    }
    
    zval *arr, **data = NULL;
    char *key = NULL;
    HashTable *arr_hash = NULL;
    HashPosition pointer;
    int array_count;
    uint num_key;
    char *string_key = NULL;

    uint key_len;
    ulong index;
    zval **myArrayProperty;

    // String 
    char * string_method = NULL;
    uint string_method_len = 0;

    
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sa",
                              &string_method,
                              &string_method_len,
                              &arr) == FAILURE) {
        RETURN_NULL();
    }

    arr_hash = Z_ARRVAL_P(arr);
    array_count = zend_hash_num_elements(arr_hash);

    std::map <std::string, std::string> args;
    std::string key_s;
    std::string val_s;
    for(zend_hash_internal_pointer_reset_ex(arr_hash, &pointer); 
        zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS; 
        zend_hash_move_forward_ex(arr_hash, &pointer)) {
        // Process keys
        if (zend_hash_get_current_key_ex(arr_hash, &key, &key_len, 
                                         &index, 0, &pointer) == HASH_KEY_IS_STRING) {
            key_s = std::string(key);
        } else {
        }

        // Process values
        if (Z_TYPE_PP(data) == IS_STRING) {
            val_s = std::string(Z_STRVAL_PP(data));
            
        }
        args[key_s] = val_s;
    }
    
    // array_init(return_value);
    // add_assoc_string(return_value, (char*)"d", (char*)"4", 1);
    std::string ret(fotolia_api->_api(string_method, args));
    RETURN_STRING(ret.c_str(), 1);
    // add_assoc_string(*myArrayProperty, "d", estrdup("4"), 1);
    // RETURN_ZVAL(arr, 1, 0);
    // RETURN_ZVAL(*myArrayProperty, 1, 0);
}

// ZEND_METHOD(Fotolia_Api, shift)
// {
// }

// ZEND_METHOD(Fotolia_Api, accelerate)
// {
//     Fotolia_Api *fotolia_api;
//     fotolia_api_object *obj = (fotolia_api_object *)zend_object_store_get_object(
//         getThis() TSRMLS_CC);
//     fotolia_api = obj->fotolia_api;
//     if (fotolia_api != NULL) {
//         fotolia_api->accelerate();
//     }
// }

// ZEND_METHOD(Fotolia_Api, brake)
// {
// }

// ZEND_METHOD(Fotolia_Api, getCurrentGear)
// {
// }

zend_function_entry fotolia_api_methods[] = {
    PHP_ME(Fotolia_Api,  __construct,         NULL, ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
    PHP_ME(Fotolia_Api,  getApiKey,           NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Fotolia_Api,  _api,                NULL, ZEND_ACC_PUBLIC)
    // PHP_ME(Fotolia_Api,  brake,           NULL, ZEND_ACC_PUBLIC)
    // PHP_ME(Fotolia_Api,  getCurrentSpeed, NULL, ZEND_ACC_PUBLIC)
    // PHP_ME(Fotolia_Api,  getCurrentGear,  NULL, ZEND_ACC_PUBLIC)
    // PHP_ME(Fotolia_Api,  print_array,     NULL, ZEND_ACC_PUBLIC)
    {NULL, NULL, NULL}
};
// ////////////

PHP_MINIT_FUNCTION(fotolia_vanilla)
{
    zend_class_entry ce;
    INIT_CLASS_ENTRY(ce, "Fotolia_Api", fotolia_api_methods);
    fotolia_api_ce = zend_register_internal_class(&ce TSRMLS_CC);
    fotolia_api_ce->create_object = fotolia_api_create_handler;
    memcpy(&fotolia_api_object_handlers,
        zend_get_std_object_handlers(), sizeof(zend_object_handlers));
    fotolia_api_object_handlers.clone_obj = NULL;
    return SUCCESS;
}

zend_module_entry fotolia_vanilla_module_entry = {
    STANDARD_MODULE_HEADER,
    PHP_FOTOLIA_VANILLA_EXTNAME,
    NULL,                       /* Functions */
    PHP_MINIT(fotolia_vanilla),        /* MINIT */
    NULL,                       /* MSHUTDOWN */
    NULL,                       /* RINIT */
    NULL,                       /* RSHUTDOWN */
    NULL,                       /* MINFO */
    PHP_FOTOLIA_VANILLA_EXTVER,
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_FOTOLIA_VANILLA
extern "C" {
ZEND_GET_MODULE(fotolia_vanilla)
}
#endif

