<?php
require 'utils.php';

// Break if not a POST
if ($_SERVER["REQUEST_METHOD"] != "POST")
    return;

// Break if no args
if (empty($_POST["words"]) || empty($_POST["apiKey"]))
    return;

// New Fotolia api if there is none or if new key
if (!isset($api) || ($_POST["apiKey"] != $api->getApiKey()))
{
    $api = new fotolia_api($_POST["apiKey"]);
}

// Get results
$results = array();
try {
    $args = array("words" => $_POST["words"], "language_id" => "2", "limit" => "5",);
    $results = json_decode($api->_api("getSearchResults", $args), $assoc = true);
    
} catch (Exception $e) {
    echo 'error : ',  $e->getMessage(), "\n";
    return;
}

// Show results. On click, popup image information
$image = array();
foreach ($results as $key => $image) {
    if (is_numeric($key)) {
?>
    <a href="#" onclick="openWithContent('<?php echo htmlspecialchars(recurseTree($image)) ?>')">
        <img src="<?php echo $image["thumbnail_url"] ?>"/> </a>
<?php
}
}
?>
