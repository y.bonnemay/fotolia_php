<?php
/** 
 * Dumb html list dump of a nested array, as in :
 * http://stackoverflow.com/questions/9542581/converting-php-array-in-html-list
 * 
 * See also : json_encode
 * 
 * @param  array  $var
 * @return string
 */
function recurseTree($var) {
    $out = '<ul>';
    foreach($var as $k => $v){
        if(is_array($v)){
            $out .= recurseTree($v);
        }else{
            $out .= '<ul>'.$k.' : '.$v.'</ul>';
        }
    }
    return $out.'</ul>';
}
?>
