<?php
require_once dirname(__FILE__) . './../php/utils.php';
class UnittestAllTheThings extends PHPUnit_Framework_TestCase
{
    /** 
     * Check recurseTree. 
     */
    public function testRecurseTree()
    {
        // elements like (1, 2, (3)) -> third is nested
        $dom = new DOMDocument();
        $dom->loadXML(recurseTree(array('1' => 1, '2' => 2, array('3' => 3))));
        
        $exp = new DOMDocument();
        $exp->loadXML("<ul><ul>1 : 1</ul><ul>2 : 2</ul><ul><ul>3 : 3</ul></ul></ul>");
        
        $this->assertEquals($dom, $exp);
    }
}
