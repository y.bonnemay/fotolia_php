# What ? #
A PHP extension to talk to the Fotolia API and its test site

# Requirements #
- PHP >= 5.4 > 7
- the usual PHP extensions dev tools
- curl extension

# How?
Run build_install.sh, then use fotolia\_vanilla.so extension as in fotolia\_php/php/display.php

# The One Badge #
![GitHub Logo](https://s-media-cache-ak0.pinimg.com/736x/75/8b/3d/758b3d06724ef10f256a8e5dab427089.jpg)


