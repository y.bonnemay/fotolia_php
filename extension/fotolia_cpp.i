/* File : fotolia_cpp.i */
%include <std_string.i>
%include <std_map.i>
%include <std_pair.i>

%template(map_string_string_impl) std::map<std::string, std::string>;

%module fotolia_cpp
    
%{
#include "fotolia_cpp.h"
%}

/* Let's just grab the original header file here */
%include "fotolia_cpp.h"

