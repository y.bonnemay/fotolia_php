<?php

# This fotolia_cpp illustrates how member variables are wrapped.

require("fotolia_cpp.php");

# ----- Object creation -----


print "Fotolia instance:\n";
$f_api = new Fotolia_Api("UwB7yPZKsRhfmsBYq5aFOEhOErVQsePC");

$f_map = new map_string_string_impl();

$f_map->set("words", "test");
$f_map->set("language_id", "2");
$f_map->set("limit", "5");

// $f_map = new map_string_string(array("words" => "test", "language_id" => "2", "limit" => "5"));

$res = json_decode($f_api->_api("getSearchResults", $f_map), TRUE);

function recurseTree($var) {
    $out = '<ul>';
    foreach($var as $k => $v){
        if(is_array($v)){
            $out .= recurseTree($v);
        }else{
            $out .= '<ul>'.$k.' : '.$v.'</ul>';
        }
    }
    return $out.'</ul>';
}

$st = recurseTree($res);
print $st;

// $f_api->printChunk();

print "Ftololia instance:\n";

print "Creating some objects:\n";
$c = new Circle(10);
print "    Created circle\n";
$s = new Square(10);
print "    Created square\n";

# ----- Access a static member -----

print "\nA total of " . Shape::get_nshapes() . " shapes were created\n";

# ----- Member data access -----

# Set the location of the object.
# Note: methods in the base class Shape are used since
# x and y are defined there.

$c->x = 20;
$c->y = 30;
$s->x = -10;
$s->y = 5;

print "\nHere is their current position:\n";
print "    Circle = ({$c->x},{$c->y})\n";
print "    Square = ({$s->x},{$s->y})\n";

# ----- Call some methods -----

# Notice how the Shape_area() and Shape_perimeter() functions really
# invoke the appropriate virtual method on each object.
print "\nHere are some properties of the shapes:\n";
foreach (array($c,$s) as $o) {
      print "    ". get_class($o) . "\n";
      print "        area      = {$o->area()}\n";
      print "        perimeter = {$o->perimeter()}\n";
}

# ----- Delete everything -----

print "\nGuess I'll clean up now\n";

# Note: this invokes the virtual destructor
$c = NULL;
$s = NULL;

# and don't forget the $o from the for loop above.  It still refers to
# the square.
$o = NULL;

print Shape::get_nshapes() . " shapes remain\n";
print "Goodbye\n";

?>
