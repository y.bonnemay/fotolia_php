#include <vector>
#include <map>
#include <algorithm> // search
#include <iostream>
#include <curl/curl.h>
#include <string.h>

static const int API_CONNECT_TIMEOUT = 30;
typedef std::map<std::string, std::string> Query;

// Written and remallocced (*memory) by curl
struct MemoryStruct
{
    char *memory;
    size_t size;
};

// Global callback passed to Curl. Write data to MemoryStruct
static size_t WriteMemoryCallback (void *ptr, size_t size, size_t nmemb, void *data)
{
    size_t realsize = size * nmemb;
    
    struct MemoryStruct * mem = (struct MemoryStruct *) data;
    mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);

    if ( mem->memory )
    {
        memcpy( &( mem->memory[ mem->size ] ), ptr, realsize );
        mem->size += realsize;
        mem->memory[ mem->size ] = 0;
    }
    return realsize;
};

class Fotolia_Api
{

public:
    // Ctors
    Fotolia_Api(const std::string & api_key);
    ~Fotolia_Api();
    
    // Functions
    std::string _api(const std::string & method, const Query & args);
    void printChunk();
    std::string getApiKey();

private:
    // Ctors
    Fotolia_Api();
    Fotolia_Api(const Fotolia_Api&);
    Fotolia_Api & operator=(const Fotolia_Api&);
    
    // Functions
    std::string _getFullURI(const std::string & method, const Query & query);
    std::string query_to_uri(const Query & query);
    std::string replaceAll( std::string const& original, std::string const& before, std::string const& after);
    std::string join_string(std::string sep, std::vector<std::string> args);
    void        loginUser_f();

    // Members
    CURL *            curl;
    MemoryStruct *    chunk;

    // Const memebers
    const std::string FOTOLIA_REST_URI;
    const std::string FOTOLIA_REST_VERSION;
    const std::string _api_key;

    // Non const members
    bool              _init_ok;
    std::string       _auth;
    std::string       _session_id;
};

class Shape {
public:
  Shape() {
    nshapes++;
  }
  virtual ~Shape() {
    nshapes--;
  }
  double  x, y;
  void    move(double dx, double dy);
  virtual double area(void) = 0;
  virtual double perimeter(void) = 0;
  static  int nshapes;
  static  int get_nshapes();
};

class Circle : public Shape {
private:
  double radius;
public:
  Circle(double r) : radius(r) { }
  ~Circle() { }
  void set_radius( double r );
  virtual double area(void);
  virtual double perimeter(void);
};

class Square : public Shape {
private:
  double width;
public:
  Square(double w) : width(w) { }
  ~Square() { }
  virtual double area(void);
  virtual double perimeter(void);
};


