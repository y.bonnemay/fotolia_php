/* File : fotolia_cpp.c */

#include "fotolia_cpp.h"
#include <math.h>
#ifndef M_PI
#  define M_PI 3.14159265358979323846
#endif


Fotolia_Api::Fotolia_Api(const std::string & api_key):
    curl(NULL),        
    FOTOLIA_REST_URI("https://api.fotolia.com/Rest"),
    FOTOLIA_REST_VERSION("1"),
    _api_key(api_key),
    _init_ok(false),
    _session_id()
{
    // Simple auth building
    _auth = _api_key + ":" + _session_id;    
    chunk = new MemoryStruct();
    
    // Init the class's handle
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    if (curl)
    {
        _init_ok = true;
    }    
};

Fotolia_Api::~Fotolia_Api()
{
    /* cleaning all curl stuff */
    curl_easy_cleanup(curl);
    
    delete chunk;
};

std::string Fotolia_Api::getApiKey()
{
    return _api_key;
}


std::string Fotolia_Api::_api(const std::string & method, const Query & args)
{
    const std::string uri(_getFullURI(method, args));
    
    /* specify password/token */
    curl_easy_setopt(curl, CURLOPT_USERPWD, _auth.c_str());

    /* send all data to this function  */ 
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
  
    /* we pass our chunk struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)chunk);

    /* set timeout */
    curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, API_CONNECT_TIMEOUT);

    // Set url before request
    curl_easy_setopt(curl, CURLOPT_URL, uri.c_str());
    
    /* Perform request */
    curl_easy_perform(curl);
    // Data is now in chunk

    return std::string((char*)chunk->memory);
}

std::string Fotolia_Api::query_to_uri(const Query & query)
{
    std::vector <std::string> dum;

    for (Query::const_iterator it = query.begin(); it != query.end(); ++it)
    {
        dum.push_back("search_parameters[" + (*it).first + "]=" + (*it).second);
    }
    std::string res(join_string("&", dum));
    res = replaceAll(res, "[", "%5B");
    return replaceAll(res, "]", "%5D");
}

std::string Fotolia_Api::_getFullURI(const std::string & method, const Query & query)
{
    const std::string search("search");
    std::string uri(FOTOLIA_REST_URI);
    uri += "/";
    uri += FOTOLIA_REST_VERSION;
    uri += "/";
    uri += search;
    uri += "/";
    uri += method;    
    uri+= "?";    
    uri+= query_to_uri(query);    
    return uri;
}

void Fotolia_Api::printChunk()
{
    printf("%s", (char*)chunk->memory);
}

std::string Fotolia_Api::join_string(std::string sep, std::vector<std::string> args)
{
    std::string out;
    for (std::vector<std::string>::iterator it = args.begin(); it != args.end(); ++it)
    {
        if (!(*it).empty())
        {
            out+= (*it);
            out+= sep;      
        }
    }
    return out.substr(0, out.size()-sep.size());
}

std::string Fotolia_Api::replaceAll( std::string const& original,
                                     std::string const& before,
                                     std::string const& after )
{
    std::string retval;
    std::string::const_iterator end     = original.end();
    std::string::const_iterator current = original.begin();
    std::string::const_iterator next    =
        std::search( current, end, before.begin(), before.end() );
    while ( next != end ) {
        retval.append( current, next );
        retval.append( after );
        current = next + before.size();
        next = std::search( current, end, before.begin(), before.end() );
    }
    retval.append( current, next );
    return retval;
}

int Shape::get_nshapes() {
  return nshapes;
}

/* Move the shape to a new location */
void Shape::move(double dx, double dy) {
  x += dx;
  y += dy;
}

int Shape::nshapes = 0;

void Circle::set_radius( double r ) {
  radius = r;
}

double Circle::area(void) {
  return M_PI*radius*radius;
}

double Circle::perimeter(void) {
  return 2*M_PI*radius;
}

double Square::area(void) {
  return width*width;
}

double Square::perimeter(void) {
  return 4*width;
}
